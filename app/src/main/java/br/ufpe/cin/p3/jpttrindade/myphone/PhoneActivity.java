package br.ufpe.cin.p3.jpttrindade.myphone;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;


public class PhoneActivity extends ActionBarActivity {

    EditText tv_numero;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_phone);

        tv_numero = (EditText) findViewById(R.id.et_numero);


    }


   public void ligar(View view){
        if(tv_numero.getText() != null) {
            String url = "tel:" + tv_numero.getText().toString();
            Intent intent = new Intent(Intent.ACTION_CALL);
            intent.setData(Uri.parse(url));
            startActivity(intent);
        }
    }



}
