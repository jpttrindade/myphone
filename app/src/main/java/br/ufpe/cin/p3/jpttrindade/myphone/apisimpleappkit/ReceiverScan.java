package br.ufpe.cin.p3.jpttrindade.myphone.apisimpleappkit;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

/**
 * Created by JP on 21/02/2015.
 */


public class ReceiverScan extends BroadcastReceiver {

    //
    private final String FACTION = "br.ufpe.cin.p3.jpttrindade.myphone.phoneactivity";
    private final String FNOME = "My Phone";

    public static final String FNOME_KEY= "fnome";
    public static final String FACTION_KEY = "faction";

    public static final String ACTION_SEND_FUNCTION = "br.ufpe.cin.p3.simpleappkit.getnewactions";


    @Override
    public void onReceive(Context context, Intent intent) {
       Intent it = new Intent(ACTION_SEND_FUNCTION);


        it.putExtra(FNOME_KEY, FNOME);
        it.putExtra(FACTION_KEY, FACTION);

        context.sendBroadcast(it);
    }
}
